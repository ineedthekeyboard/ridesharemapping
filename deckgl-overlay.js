import React, {Component} from 'react';
import DeckGL, {PolygonLayer} from 'deck.gl';

import TripsLayer from './trips-layer';

const LIGHT_SETTINGS = {
  lightsPosition: [-97.74, 30.26, 8000, -96.74, 31, 5000],
  ambientRatio: 0.05,
  diffuseRatio: 0.6,
  specularRatio: 0.8,
  lightsStrength: [2.0, 0.0, 0.0, 0.0],
  numberOfLights: 2
};

export default class DeckGLOverlay extends Component {

  static get defaultViewport() {
    return {
      longitude: -97.74,
      latitude: 30.26,
      zoom: 12,
      maxZoom: 16,
      pitch: 45,
      bearing: 0
    };
  }

  _initialize(gl) {
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);
  }

  render() {
    const {viewport, buildings, trips, trailLength, time} = this.props;

    if (!buildings || !trips) {
      return null;
    }

    const layers = [
      new TripsLayer({
        id: 'trips',
        data: trips,
        getPath: d => d.segments,
        getColor: d => {
          var color = [];
          switch (d.vendor) {
            case "0":
              color = [255,255,255];
              break;
            case "1":
              color = [255,255,0];
              break;
            case "2":
            case "3":
            case "4":
              color = [255,51,51];
              break;
            default:
              color = [255,51,51];
              break;
          }
          return color
        },
        opacity: 0.9,
        strokeWidth: 25,
        trailLength,
        currentTime: time
      })
      // }),
      // new PolygonLayer({
      //   id: 'buildings',
      //   data: buildings,
      //   extruded: true,
      //   wireframe: false,
      //   fp64: true,
      //   opacity: 0.5,
      //   getPolygon: f => f.polygon,
      //   getElevation: f => f.height,
      //   getFillColor: f => [74, 80, 87],
      //   lightSettings: LIGHT_SETTINGS
      // })
    ];

    return (
      <DeckGL {...viewport} layers={layers} onWebGLInitialized={this._initialize} />
    );
  }
}
