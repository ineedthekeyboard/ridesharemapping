/* global window,document */
import React, {Component} from 'react';
import {render} from 'react-dom';
import MapGL from 'react-map-gl';
import DeckGLOverlay from './deckgl-overlay.js';
import {json as requestJson} from 'd3-request';
import Slider from 'react-rangeslider';
import Toggle from 'react-toggle';
// To include the default styles
import 'react-rangeslider/lib/index.css'
import style from './style.css'

//Set up specifics for this data set
const MAPBOX_TOKEN = 'pk.eyJ1IjoiaW5lZWR0aGVrZXlib2FyZCIsImEiOiJjajFjamUzd3owMDNwMndtaG5hZzhkc3hqIn0.QaWsHgx0Yx315h9dDt0m4g'; // eslint-disable-line
var loopTime1 = 250000
const loopLength1 = 604800
const maxTime = 604800
//Set up specifics for this data set
const week = '43'
const markStartTime = 1477281609.0
const origTimeStamp = "2016-11-24 00:00:09"

class Root extends Component {

  constructor(props) {
    super(props);
    this.state = {
      viewport: {
        ...DeckGLOverlay.defaultViewport,
        width: 500,
        height: 500
      },
      buildings: null,
      trips: null,
      time: 0,
      timeSpeed: 15,
      speed: 2,
      hideShow: false,
      hideShowState: 'block'
    };

    requestJson('./data2/buildings.json', (error, response) => {
      if (!error) {
        this.setState({buildings: response});
      }
    });

    requestJson('./data2/week'+week+'-final.json', (error, response) => {
      if (!error) {
        this.setState({trips: response});
      }
    });
  }

  componentDidMount() {
    window.addEventListener('resize', this._resize.bind(this));
    //window.addEventListener('click', this._animate.bind(this, true));
    this._resize();
    this._animate();
  }

  componentWillUnmount() {
    if (this._animation) {
      window.cancelAnimationFrame(this._animationFrame);
    }
  }

  _animate() {
    let timestamp = Date.now();
    const loopLength = loopLength1;
    // console.log(this.state.timeSpeed)
    var loopTime = loopTime1 * this.state.timeSpeed;
    this.setState({
      time:((timestamp % loopTime) / loopTime) * loopLength
      //time: 0 
    });
    this._animationFrame = window.requestAnimationFrame(this._animate.bind(this));
  }

  _resize() {
    this._onChangeViewport({
      width: window.innerWidth,
      height: window.innerHeight
    });
  }

  _onChangeViewport(viewport) {
    this.setState({
      viewport: {...this.state.viewport, ...viewport}
    });
  }

  _handleVolumeChange(value) {
    this.setState({
      time: value
    })
  }

  _formatHandle(value) {
    // console.log(value)
    // todo compute the tooltip here
    value = Math.round((value/(60*60*24))*10)/10 + " Days"
    return value;
  }

  _formatDate(currentTime) {
    var originalTime = (new Date(origTimeStamp)).getTime();
    var currentDate = new Date(originalTime + (currentTime*1000)).getTime();
    currentDate = new Date(currentDate);
    var d = currentDate;
    var datestring = ("0" + d.getMonth()).slice(-2) + "-" + ("0"+(d.getDate()+1)).slice(-2) + "-" + d.getFullYear();
    return datestring;
  }
  _formatTime(currentTime) {
    var originalTime = (new Date(origTimeStamp)).getTime();
    var currentDate = new Date(originalTime + (currentTime*1000)).getTime();
    currentDate = new Date(currentDate);
    var d = currentDate;
    var datestring = ("0" + d.getHours()).slice(-2) + ":" + ("0" + d.getMinutes()).slice(-2);
    return datestring;
  }
  _changeTimeSpeed(e, speed, listedSpeed) {
    // loopTime1 = 250000 * speed;
    this.setState({
      timeSpeed:speed,
      speed: listedSpeed
    })
    console.log('looptime: ' + speed);
  }
  _handleHideShow() {
    console.log(this.state);
    this.setState({
      hideShow: !this.state.hideShow
    });
    if (this.state.hideShow) {
      this.setState({
        hideShowState: 'block'
      });
    } else {
     this.setState({
        hideShowState: 'none'
      });
    }
    
  }
  render() {
    const {viewport, buildings, trips, time, timeSpeed, hideShow, hideShowState} = this.state;
    //console.log(time);
    return (
      <MapGL
        {...viewport}
        mapStyle="mapbox://styles/ineedthekeyboard/cj1gz0zho001e2rpivbj07nbd"
        perspectiveEnabled={true}
        onChangeViewport={this._onChangeViewport.bind(this)}
        mapboxApiAccessToken={MAPBOX_TOKEN}>
        <DeckGLOverlay viewport={viewport}
          buildings={buildings}
          trips={trips}
          trailLength={180}
          time={time}
          />
          <label>
            <Toggle
              defaultChecked={true}
              onChange={this._handleHideShow.bind(this)} />
          </label>
          <div className='panel' style={{display:hideShowState}}>
          <div className="timeDisplay newSize"><span className="time1">{this._formatDate(time)}</span><br></br><span className="time2">{this._formatTime(time)}</span></div>
          <div className='timeTitle'>Time Speed: {this.state.speed}x</div>
          <div className='timeSpeed'>
            <button onClick={(e) => this._changeTimeSpeed(e, 30, 1)}>1x</button>
            <button onClick={(e) => this._changeTimeSpeed(e, 15, 2)}>2x</button>
            <button onClick={(e) => this._changeTimeSpeed(e, 5, 3)}>3x</button>
            <button onClick={(e) => this._changeTimeSpeed(e, .50, 5)}>5x</button>
          </div>
          <div className='timeDisplay'>Elapsed Days: {Math.round((time/(60*60*24))*10)/10 + " Days"}</div>
            <Slider
              value={time}
              max={maxTime}
              orientation="horizontal"
              tooltip = {true}
              format={this._formatHandle.bind(this)}
              onChange={this._handleVolumeChange.bind(this)}
            />
            
          </div>
      </MapGL>
      
    );
  }
}

render(<Root />, document.body.appendChild(document.createElement('div')));
