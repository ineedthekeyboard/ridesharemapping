import csv, ujson, requests

# read the raw csv into a python list
rawdata = open('rawdata/RA_Full.csv', 'r')
rawdata = csv.reader(rawdata)
rawdata = list(rawdata)

#strip out the unneccessary data from the current list and save it down to a new json file:
processedData = list()
newObj = {
    "endTime":rawdata[0][0],
    "startTime":rawdata[0][4],
    "startLat":rawdata[0][14],
    "startLong":rawdata[0][13],
    "endLat":rawdata[0][2],
    "endLong":rawdata[0][3]
}
processedData.append(newObj)
for row in rawdata[360000:455529]:
    newObj = {
        "endTime":row[0],
        "startTime":row[4],
        "startLat":row[14],
        "startLong":row[13],
        "endLat":row[2],
        "endLong":row[3]
    }
    processedData.append(newObj)
counter = 0
totalnum = len(processedData)
del rawdata

for row in processedData[1:]:
    counter += 1
    url = 'https://router.project-osrm.org/route/v1/driving/'+ row['startLong'] +','+ row['startLat'] +';'+ row['endLong'] +','+ row['endLat'] +'?geometries=polyline&steps=true'
    print(counter, ' of ', totalnum, ' - ', url)
    response = requests.get(url)
    newSteps = list()
    try:
        steps = ujson.loads(response.text)
        for coords in steps['routes'][0]['legs'][0]['steps']:
            newObj = {
                'distance': coords['distance'],
                'weight': coords['weight'],
                'duration': coords['duration'],
                'longStep':  coords['maneuver']['location'][0],
                'latStep': coords['maneuver']['location'][1]
            }
            newSteps.append(newObj)
    except:
        print('error')
    row['segments'] = newSteps
    if counter % 10000 == 0:
        with open('backups/firstHalfbk'+ str(counter) +'.json', 'w') as f:
            ujson.dump(processedData[:counter], f)
            print('saved at', counter)
with open('rawdata/firstHalffinaloutput.json', 'w') as f:
    ujson.dump(processedData, f)
    print('saved at', counter)