import csv

#Get data with some filters applied
def getstuff(filename, criterion):
    with open(filename, "r") as csvfile:
        datareader = csv.reader(csvfile)
        count = 0
        for row in datareader:
            #print('inner count', count)
            count += 1
            processedRow = {                      
                "startTime":row[0],
                "startLat":row[13],
                "startLong":row[12],
                "endTime":row[2],
                "endLat":row[4],
                "endLong":row[5]
            }
            # startime = 0
            # endtime =  2
            # startLat = 13
            # startLong = 12
            # endLat = 4
            # endLong = 5

            if (count > 0 and count < 5):
                yield processedRow
            else:
                continue
#Loop over actual data here
count = 0
for row in getstuff("rawdata/RA_Full_4_13_2017.csv", "test"):
    print(count)
    print(row)
    count += 1