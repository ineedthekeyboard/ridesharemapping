var config = module.exports
var PRODUCTION = process.env.NODE_ENV === 'production'

config.isDevelopment = !PRODUCTION;
config.isDebug = process.env.NODE_ENV === 'debug';

config.WORKERS = process.env.WEB_CONCURRENCY || 1;
config.express = {
    sslPort: 443,
    port: process.env.PORT || 3000,
    ip: process.env.IP || '127.0.0.1'
}

if (PRODUCTION) {
    config.express.ip = '0.0.0.0'
    config.mongodb = {
        uri: process.env.MONGODB_URI
    }
    config.redis = {
        uri: process.env.REDIS_URL
    }
}