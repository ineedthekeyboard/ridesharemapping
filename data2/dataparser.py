import csv, ujson, requests
filename = 'week43'
# read the raw csv into a python list
rawdata = open(filename + '.csv', 'r')
rawdata = csv.reader(rawdata)
rawdata = list(rawdata)

#strip out the unneccessary data from the current list and save it down to a new json file:
processedData = list()
# newObj = {
#     "endTime":rawdata[0][0],
#     "startTime":rawdata[0][4],
#     "startLat":rawdata[0][14],
#     "startLong":rawdata[0][13],
#     "endLat":rawdata[0][2],
#     "endLong":rawdata[0][3]
# }
# processedData.append(newObj)
for row in rawdata[:]:
    newObj = {
        "endTime":row[1],
        "startTime":row[0],
        "surgefactor":row[2],
        "surgebin": row[3],
        "startLat":row[4],
        "startLong":row[5],
        "endLat":row[6],
        "endLong":row[7]
    }
    processedData.append(newObj)
counter = 0
totalnum = len(processedData)
del rawdata

for row in processedData[1:]:
    counter += 1
    url = 'https://router.project-osrm.org/route/v1/driving/'+ row['startLong'] +','+ row['startLat'] +';'+ row['endLong'] +','+ row['endLat'] +'?geometries=polyline&steps=true'
    print(counter, ' of ', totalnum, ' - ', url)
    response = requests.get(url)
    newSteps = list()
    try:
        steps = ujson.loads(response.text)
        for coords in steps['routes'][0]['legs'][0]['steps']:
            newObj = {
                'distance': coords['distance'],
                'weight': coords['weight'],
                'duration': coords['duration'],
                'longStep':  coords['maneuver']['location'][0],
                'latStep': coords['maneuver']['location'][1]
            }
            newSteps.append(newObj)
    except:
        print('error')
    row['segments'] = newSteps
    if counter % 10000 == 0:
        with open('output/firstHalfbk'+ str(counter) +'-' + filename +'.json', 'w') as f:
            ujson.dump(processedData[:counter], f)
            print('saved at', counter)
with open('output/' + filename + '.json', 'w') as f:
    ujson.dump(processedData, f)
    print('saved at', counter)