#*********************************************************
#*** Convert sample of full json to specific map format **
#*********************************************************

import ijson, io, ujson, datetime, time
from operator import itemgetter, attrgetter, methodcaller
from random import randint
filename = 'week11';
startep = 1489723200.0;
startep2 =1489723200000;
#Stream Trips Data
trips = ijson.items(io.open('paredDown/' + filename + '.json', 'rb'), 'item')

#Setup Output file
processedData = list()
timeLineData = {
    "earliestStartEpoch": float(),
    "earliestStartReadable":'',
    "latestStartEpoch": float(),
    "latestStartReadable":'',
    "earliestEndEpoch": float(),
    "earliestEndReadable":'',
    "latestEndEpoch": float(),
    "latestEndReadable":''
}
#for processing dates into python datetime obj: 
def getEpochSeconds(timeToProcess):
    epoch = datetime.datetime.strptime(timeToProcess, '%Y-%m-%d %H:%M:%S')
    epoch = time.mktime(epoch.timetuple())
    return float(epoch)

def calculateRangeOfStartTimes(startTime):
    if startTime < timeLineData['earliestStartEpoch'] and startTime != 0:
        timeLineData['earliestStartEpoch'] = startTime
        timeLineData['earliestStartReadable'] = datetime.datetime.fromtimestamp(startTime).strftime('%Y-%m-%d %H:%M:%S')
    if startTime > timeLineData['latestStartEpoch']:
        timeLineData['latestStartEpoch'] = startTime
        timeLineData['latestStartReadable'] = datetime.datetime.fromtimestamp(startTime).strftime('%Y-%m-%d %H:%M:%S')

def calculateRangeOfEndTimes(endTime):
    if endTime < timeLineData['earliestEndEpoch'] and endTime != 0:
        timeLineData['earliestEndEpoch'] = endTime
        timeLineData['earliestEndReadable'] = datetime.datetime.fromtimestamp(endTime).strftime('%Y-%m-%d %H:%M:%S')
    if endTime > timeLineData['latestEndEpoch']:
        timeLineData['latestEndEpoch'] = endTime
        timeLineData['latestEndReadable'] = datetime.datetime.fromtimestamp(endTime).strftime('%Y-%m-%d %H:%M:%S')

def processSegments(segments, sTime, eTime):
  newSegments = list()
  countOfSegments = len(segments)
  
  if countOfSegments > 0:
    durationInterval = (eTime - sTime) / countOfSegments
    #durationInterval = 2500000 / countOfSegments
  else:
      durationInterval = 0
  duration = sTime
  for idx, seg in enumerate(segments):
    newSegments.append([str(seg['longStep']), str(seg['latStep']), str(duration)])
    duration += durationInterval
  #newSegments = sorted(newSegments, key=itemgetter(2))
  return newSegments

def processTrip(idx, trip):
    startTimeEpoch = getEpochSeconds(trip['startTime'])
    endTimeEpoch = getEpochSeconds(trip['endTime'])
    if idx == 1:
        timeLineData['earliestStartEpoch'] = startTimeEpoch
        timeLineData['earliestStartReadable'] = datetime.datetime.fromtimestamp(startTimeEpoch).strftime('%Y-%m-%d %H:%M:%S')
        timeLineData['latestStartEpoch'] = startTimeEpoch
        timeLineData['latestStartReadable'] = datetime.datetime.fromtimestamp(startTimeEpoch).strftime('%Y-%m-%d %H:%M:%S')
        timeLineData['earliestEndEpoch'] = endTimeEpoch
        timeLineData['earliestEndReadable'] = datetime.datetime.fromtimestamp(endTimeEpoch).strftime('%Y-%m-%d %H:%M:%S')
        timeLineData['latestEndEpoch'] = endTimeEpoch
        timeLineData['latestEndReadable'] = datetime.datetime.fromtimestamp(endTimeEpoch).strftime('%Y-%m-%d %H:%M:%S')

    calculateRangeOfStartTimes(startTimeEpoch)
    calculateRangeOfEndTimes(endTimeEpoch)
    sTime = startTimeEpoch - startep
    eTime = endTimeEpoch - startep
    #comment out the below to just do time calculations
    segments = processSegments(trip['segments'], sTime, eTime)
    newObj = {
        "vendor":trip['surgeBin'],
        # "vendor": randint(0,4),
        "startTime":sTime,
        "endTime":eTime,
        "segments":segments
    }
    processedData.append(newObj)

#go through each trip
for idx, trip in enumerate(trips):
    if idx % 5000 == 0:
        print(idx)
    # print(mmmmmm)
    if (datetime.datetime.strptime(trip['startTime'], '%Y-%m-%d %H:%M:%S').timestamp()) > 1489723200 and (datetime.datetime.strptime(trip['startTime'], '%Y-%m-%d %H:%M:%S').timestamp()) < 1489852800:
        print(idx)
        if (trip['endLong'] != 'end_location_long'):
            processTrip(idx, trip)
        else:
            print(idx, trip)

#when the final object is done dump it:
with open(filename + '-sp-timelineData.json', 'w') as td:
    ujson.dump(timeLineData, td)
with open(filename + '-sp-final.json', 'w') as f:
    ujson.dump(processedData, f)
